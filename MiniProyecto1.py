import pymysql #importamos el connector de python con mysql
#creamos la conexion con la base de datos
conexion = pymysql.connect(
    host = 'localhost',
    user = 'root',
    passwd = '123456',
    database = 'infoaeropuertos'
)
#creamos un cursor
cursor = conexion.cursor()
try:
    # creamos un arreglo de tuplas con los datos a insertar
    aeropuertos = [
        (39340, 'SHCC', 'heliport', 'Clinica Las Condes Heliport', 2461, 'Santiago', 25),
        (39379, 'SHMA', 'heliport', 'Clinica Santa Maria Heliport', 2028, 'Santiago', 25),
        (39390, 'SHPT', 'heliport', 'Portillo Heliport', 9000, 'Los Andes', 25)
    ]
    # insertamos datos en bloque

    cursor.executemany("INSERT INTO aeropuertos VALUES(%s,%s,%s,%s,%s,%s,null,%s)", aeropuertos)
    print("Datos insertados correctamente")
    # guardamos la consulta
    conexion.commit()
except pymysql.err.IntegrityError:
    print("*************************************************************************")
    print("Alguno de los datos que se intenta insertar, ya esta en la base de datos")
    print("*************************************************************************")
#realizamos una consulta don la altura de los aeropuertos sea mayor a 5000
print("\n")
cursor.execute("""
                SELECT 
                name,type,municipality,elevation_ft
                FROM aeropuertos
                WHERE
                elevation_ft > 5000
               """)
print("---------------------------------------------")
for aeropuerto in cursor.fetchall():
    print(f"Aeropuerto: {aeropuerto[0]}")
    print(f"Tipo: {aeropuerto[1]}")
    print(f"Municipalidad: {aeropuerto[2]}")
    print(f"Altura: {aeropuerto[3]}\n")
    print("---------------------------------------------")