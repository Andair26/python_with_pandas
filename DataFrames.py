import pymysql
import pandas as pd
conexion = pymysql.connect(
    host="localhost",
    database="cine",
    user="root",
    passwd="123456"
)
query_sql = ("""
    SELECT movies.name,movies.year, directors.first_name, movies.rank from movies
    JOIN movies_directors
    ON movies.id = movies_directors.movie_id
    JOIN directors
    ON movies_directors. director_id = directors.id
    WHERE movies.rank > 8
    ORDER BY movies.rank DESC
""")
df1 = pd.read_sql_query(query_sql,conexion)
#print(df)
df1 = df1.rename({
    'name':'Pelicula',
    'year':'Agno',
    'first_name':'Director',
    'rank':'Puntaje'
},axis='columns')
print("---------------------------------------------------")
print("            DataFrame original                     ")
print("---------------------------------------------------")
print(df1)
df2 = df1.loc[0:9,['Pelicula','Puntaje']]
print("---------------------------------------------------")
print("            DataFrame Recortado                    ")
print("---------------------------------------------------")
print(df2)
df3 = df1.iloc[20:50]
print("---------------------------------------------------")
print("     DataFrame de la linea 20 a 50                 ")
print("---------------------------------------------------")
print(df3)
