from sqlalchemy import create_engine
import pandas as pd
db = "cine"
tables = [
    "movies",
    "actors",
    "directors",
    "movies_actors",
    "movies_directors"
]
paths = [
    "C:/Users/Usuario/Downloads/tarea/movies.csv",
    "C:/Users/Usuario/Downloads/tarea/actors.csv",
    "C:/Users/Usuario/Downloads/tarea/directors.csv",
    "C:/Users/Usuario/Downloads/tarea/movies_actors.csv",
    "C:/Users/Usuario/Downloads/tarea/movies_directors.csv"
]
url = "mysql+mysqlconnector://root:123456@localhost/"
engine = create_engine(url + db, echo = False)
#leemos el csv y lo alamacenamos en un data frame
dfs = []
for path in paths:
    dfs.append(pd.read_csv(path,sep=';'))
#insertamos los datos en al base de datos mediante to_sql
cont = 0
for df in dfs:
    df.to_sql(name=tables[cont],con=engine,if_exists='append')
    cont += 1