import pymysql

conexion = pymysql.connect(
    host="localhost",
    database="cine",
    user="root",
    passwd="123456"
)
cursor = conexion.cursor()
print("---------------------------------------------------")
print("       Directors con mas de 3 peliculas            ")
print("---------------------------------------------------")
cursor.execute("""
    SELECT directors.first_name,directors.last_name,COUNT(movies.name) FROM directors
    JOIN movies_directors
    ON directors.id = movies_directors.director_id
    JOIN movies
    ON movies_directors.movie_id = movies.id
    GROUP BY(directors.first_name)
    HAVING COUNT(movies.name) > 3
    ORDER BY COUNT(movies.name) DESC
""")
directors = cursor.fetchall()
for director in directors:
    print(f"Nombre Completo: {director[0]} {director[1]}, Cantidad de peliculas: {director[2]}")

print("---------------------------------------------------")
print("       Actores            ")
print("---------------------------------------------------")
cursor.execute("""
    SELECT actors.first_name,actors.last_name, COUNT(movies.name) FROM actors
    JOIN movies_actors
    ON actors.id = movies_actors.actor_id
    JOIN movies
    ON movies_actors.movie_id = movies.id
    GROUP BY(actors.first_name)
    ORDER BY COUNT(actors.last_name) ASC
""")
actors = cursor.fetchall()
for actor in actors:
    print(f"Nombre Completo: {actor[0]} {actor[1]}, Cantidad de peliculas: {actor[2]}")

print("---------------------------------------------------")
print("       Ranking de Peliculas                        ")
print("---------------------------------------------------")

cursor.execute("""
    SELECT movies.name,movies.year, CONCAT(directors.first_name," ",directors.last_name), movies.rank FROM movies
    JOIN movies_directors
    ON movies.id = movies_directors.movie_id
    JOIN directors
    ON movies_directors.director_id = directors.id
    WHERE movies.rank > 8
    ORDER BY movies.rank DESC
""")

movies = cursor.fetchall()
for movie in movies:
    print(f"Pelicula: {movie[0]}, Año: {movie[1]}, Director: {movie[2]}, Ranking: {movie[3]}")